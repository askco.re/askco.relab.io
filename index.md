<div align="center">

![askco.re ontologies](assets/images/banner.png)
</div>



Ontologies
----------

* [http://askco.re/db](http://askco.re/db) database ontology ([askcore_db.ttl](https://gitlab.com/askco.re/ontologies/-/blob/stable/askcore_db.ttl?ref_type=heads))
* [http://askco.re/dataset](http://askco.re/dataset) database ontology ([askcore_dataset.ttl](https://gitlab.com/askco.re/ontologies/-/blob/stable/askcore_dataset.ttl?ref_type=heads))
